Start server with the command `./gradlew bootRun`

REST API endpoints:

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
POST | /customers/new | {"firstName": "*firstName*", "lastName": "*lastName*" } | *customerNumber* | Adds new customer to database
GET | /customers/*number* | | {"number" : *number*, "firstName": "*firstName*", "lastName": "*lastName*"} | Returns customer with given number
GET | /customers/key | {"firstName": "*firstName*", "lastName": "*lastName*"} |*customerNumber*| Returns customer number for this customer
GET | /customers/ | | {"number" : *number*, "firstName": "*firstName*", "lastName": "*lastName*"} | Returns all customer in the database
PUT | /customers/*number* |{"firstName": "*firstName*", "lastName": "*lastName*"} |*Customer Updated* | Updates a customer based on customer number
POST | /products/new | {"price": *price*, "description": *description*} | *sku* | Adds new product to database
GET | /products/*sku* | | {"sku": *sku*, "price": *price*, "description": *description*} | Returns product with given number
POST | /orders/new/*number* | | *orderNumber* | Adds new order to database for this customer
GET | /orders/*number*/customer | | *customerNumber* | Returns customer for this order number
GET | /orders/customers/*number* | | *orderNumber* | Returns a list of orders for this customer number
GET | /orders/*number*/lines | | [{"sku": *sku*, "quantity": *quantity*}, ...] | Returns array of order lines for this order
PUT | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Increase quantity of the product in this order
DELETE | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Decrease quantity of the product in this order
POST | /shutdown | | | Shuts down the server